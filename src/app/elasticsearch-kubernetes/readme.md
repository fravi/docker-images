
# this is elasticsearch for kubernetes cluster, with delete-by-query plugin installed
  ( required by heapster data structure which is not subdivided into date-range-indexes )
  note: heapster->logstash->elasticsearch could be a viable alternative in the future  

- https://www.elastic.co/guide/en/elasticsearch/plugins/current/plugins-delete-by-query.html

- This project started as fork of 
 - https://github.com/pires/docker-elasticsearch-kubernetes
 
# Licence
 licence of other projects is inherited 


statsd-elasticsearch-docker